require "rails_helper"

RSpec.describe MarkSheetsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/mark_sheets").to route_to("mark_sheets#index")
    end

    it "routes to #show" do
      expect(:get => "/mark_sheets/1").to route_to("mark_sheets#show", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/mark_sheets").to route_to("mark_sheets#create")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/mark_sheets/1").to route_to("mark_sheets#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/mark_sheets/1").to route_to("mark_sheets#destroy", :id => "1")
    end

  end
end
