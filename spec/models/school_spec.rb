require 'rails_helper'

RSpec.describe School, type: :model do
  it 'is valid with valid attributes' do
    expect(School.new(school_name: 'Little Flower', school_place: 'Andavoorani')).to be_valid
  end
end
