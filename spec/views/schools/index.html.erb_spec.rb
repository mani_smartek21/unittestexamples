require 'rails_helper'

RSpec.describe "schools/index", type: :view do
  before(:each) do
    assign(:schools, [
      School.create!(
        :school_name => "School Name",
        :school_place => "School Place"
      ),
      School.create!(
        :school_name => "School Name",
        :school_place => "School Place"
      )
    ])
  end

  it "renders a list of schools" do
    render
    assert_select "tr>td", :text => "School Name".to_s, :count => 2
    assert_select "tr>td", :text => "School Place".to_s, :count => 2
  end
end
