require 'rails_helper'

RSpec.describe "schools/show", type: :view do
  before(:each) do
    @school = assign(:school, School.create!(
      :school_name => "School Name",
      :school_place => "School Place"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/School Name/)
    expect(rendered).to match(/School Place/)
  end
end
