require 'rails_helper'

# Negative
RSpec.describe "the signin process", type: :feature do
  it "signin with negative scenario" do
    visit '/users/sign_in'
    within("#new_user") do
      fill_in 'Email', with: 'test@domain.com'
      fill_in 'Password', with: '54das65d46s'
    end
    click_button 'Log in'
    save_and_open_page
    expect(page).to have_content 'Manage School'
  end
end