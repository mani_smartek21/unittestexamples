require 'rails_helper'

# Positive
RSpec.describe "the signin process", type: :feature do
  before :each do
    User.create(email: 'manikandana@smartek21.com', password: '12345678', password_confirmation: '12345678')
  end
  it "signin with positive scenario" do
    visit '/users/sign_in'
    within("#new_user") do
      fill_in 'Email', with: 'manikandana@smartek21.com'
      fill_in 'Password', with: '12345678'
    end
    click_button 'Log in'
    save_and_open_page
    expect(page).to have_content 'Manage School'
  end
end