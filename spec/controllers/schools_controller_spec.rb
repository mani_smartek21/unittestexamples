require 'rails_helper'

# Automation Example
RSpec.describe SchoolsController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # School. As you add validations to School, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    {school_name: "Don Bosco", school_place: "Perambur"}
  }

  let(:invalid_attributes) {
    {name: "Don Bosco", place: "Perambur"}
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # SchoolsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      school = School.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      school = School.create! valid_attributes
      get :show, params: {id: school.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      school = School.create! valid_attributes
      get :edit, params: {id: school.to_param}, session: valid_session
      expect(response).to be_success
    end
  end


  #  Positive Approach
  describe "POST #create" do
    context "with valid params" do
      it "creates a new School" do
        expect {
          post :create, params: {school: valid_attributes}, session: valid_session
        }.to change(School, :count).by(1)
      end

      it "redirects to the created school" do
        post :create, params: {school: valid_attributes}, session: valid_session
        expect(response).to redirect_to(School.last)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {school: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end


  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {school_name: "Don Bosco", school_place: "Perambur"}
      }

      it "updates the requested school" do
        school = School.create! valid_attributes
        put :update, params: {id: school.to_param, school: new_attributes}, session: valid_session
        school.reload
      end

      it "redirects to the school" do
        school = School.create! valid_attributes
        put :update, params: {id: school.to_param, school: valid_attributes}, session: valid_session
        expect(response).to redirect_to(school)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        school = School.create! valid_attributes
        put :update, params: {id: school.to_param, school: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested school" do
      school = School.create! valid_attributes
      expect {
        delete :destroy, params: {id: school.to_param}, session: valid_session
      }.to change(School, :count).by(-1)
    end

    it "redirects to the schools list" do
      school = School.create! valid_attributes
      delete :destroy, params: {id: school.to_param}, session: valid_session
      expect(response).to redirect_to(schools_url)
    end
  end

end
