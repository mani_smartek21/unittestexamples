Rails.application.routes.draw do
  # For details on the DSL available wit
  # hin this file, see http://guides.rubyonrails.org/routing.html

  devise_scope :user do
    authenticated :user do
      root 'schools#index', as: :authenticated_root
    end
    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  devise_for :users

  get 'home', to: 'static_page#index'
  get 'about', to: 'static_page#about'
  get 'contact', to: 'static_page#contact'

  get 'users', to: 'static_page#users'

  resources :student_lists

  get 'mark_sheets', to: 'mark_sheets#index'
  get 'mark_sheets/:id', to: 'mark_sheets#show'
  post 'mark_sheets', to: 'mark_sheets#create'
  patch 'mark_sheets/:id', to: 'mark_sheets#update'
  delete 'mark_sheets/:id', to: 'mark_sheets#destroy'

  resources :schools
end
