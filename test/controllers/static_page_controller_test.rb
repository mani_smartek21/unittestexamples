require 'test_helper'


# This File Represents: Test the URL accessible or not

class StaticPageControllerTest < ActionDispatch::IntegrationTest
  test "should get root_url" do
    get home_url
    assert_response :success
    assert_select "title", "Home - Unit Test" # Expected is success Example
  end

  test "should get about" do
    get about_url
    assert_response :success
    assert_select "title", "About - Unit Test" # Expected is success Example

    # assert_select "title", "About" # Expected is failure Example
  end

  test "should get contact" do
    get contact_url
    assert_response :success
    assert_select "title", "Contact - Unit Test" # Expected is success Example
  end

end
