class CreateStudentLists < ActiveRecord::Migration[5.2]
  def change
    create_table :student_lists do |t|
      t.string :student_name
      t.string :student_class
      t.string :student_std

      t.timestamps
    end
  end
end
