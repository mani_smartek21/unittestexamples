Rails Unit Test Examples
=========================

## Test Unit Examples using rspec-rails, capybara, launchy, pry-rails and database_cleaner gems.

Env Setup with version
----------------------

  Ruby 2.5.1p57 (2018-03-29 revision 63029) [x86_64-linux]

  Rails 5.2.0

App Run
-----------

  bundle install # if need to install dependencies

  rake db:migrate RAILS_ENV=test

  rails server -b 192.168.1.100 -p 3000 -e development

Unit Test Run
-------------

  rake spec
  
  rspec spec/requests/login_failure_spec.rb # Automation Request
  
  rspec spec/requests/login_success_spec.rb # Automation Request
  
  rspec spec/controllers/schools_controller_spec.rb
