class StaticPageController < ApplicationController
  def index
  end

  def about
  end

  def contact
  end

  def users
    redirect_to :action => :index
  end
end
