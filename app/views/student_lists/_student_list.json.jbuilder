json.extract! student_list, :id, :student_name, :student_class, :student_std, :created_at, :updated_at
json.url student_list_url(student_list, format: :json)
