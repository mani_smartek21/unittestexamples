json.extract! school, :id, :school_name, :school_place, :created_at, :updated_at
json.url school_url(school, format: :json)
