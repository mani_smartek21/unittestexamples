class StudentList < ApplicationRecord
  has_many :mark_sheets, dependent: :destroy

  validates :student_name, presence: true
  validates :student_class, presence: true
  validates :student_std, presence: true
end
