class School < ApplicationRecord

  validates :school_name, presence: true
  validates :school_place, presence: true
end
