class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable, :recoverable

  validates :email, email: true

  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable
end
